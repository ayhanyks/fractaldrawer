#ifndef SIZE_LEVEL_H
#define SIZE_LEVEL_H

#include <QWidget>
#include <imagevis.h>

namespace Ui {
class size_level;
}


class size_level : public QWidget
{
    Q_OBJECT

public:
    explicit size_level(imageVis*imgvis, QWidget *parent = 0);
    ~size_level();

private slots:
    void on_pushButton_clicked();

    void on_horizontalSlider_valueChanged(int value);

private:
    imageVis*imgvis;
    Ui::size_level *ui;

    uint32_t log2x(uint32_t x)
    {
        uint32_t logx=0;
        while(x)
        {
            logx++;
            x=x>>1;
        }
        logx--;
        return logx;
    }



};

#endif // SIZE_LEVEL_H
