#include "mainw.h"
#include "qmenubar.h"
#include "qdebug.h"

#define XRES    (1024)
#define YRES    (512)

mainW::mainW(QWidget *parent) : QMainWindow(parent)
{



    QFrame*frm=new QFrame(this);
    this->setCentralWidget(frm);
    this->resize(XRES+100,YRES+100);
    cl=new clickLabel(frm);
    QScrollArea*qsa=new QScrollArea(frm);

    qsa->setBackgroundRole(QPalette::Dark);
    QHBoxLayout*qhbl=new QHBoxLayout();
    frm->setLayout(qhbl);
    imgvis=new imageVis(qsa);
    imgvis->setup(XRES,YRES,256);
    qsa->setWidget(imgvis->getScren());
    qhbl->addWidget(qsa);
    createMenus();

}



void mainW::createMenus(){



    QMenuBar *menu = this->menuBar();
    QMenu *image = new QMenu("image");
    QMenu *resize = new QMenu("resize");


    for (auto mstr:menu_image)
        resize->addAction(mstr);

    image->addMenu(resize);
    connect(resize,SIGNAL(triggered(QAction*)),this,SLOT(menu_resize(QAction*)));


    QMenu *levels = new QMenu("levels");

    for (auto lstr:menu_levels)
    {
        QString item_text=lstr;
        if (QString::number(imgvis->histogramGetLevels())==lstr)
        {
            item_text="*"+item_text;
        }

        levels->addAction(item_text);

    }

    connect(levels,SIGNAL(triggered(QAction*)),this,SLOT(menu_set_levels(QAction*)));


    image->addMenu(levels);


    menu->addMenu(image);

}


void mainW::menu_resize(QAction*action){

    QString S=action->text();

    int W = S.split("x")[0].toInt();
    int H = S.split("x")[1].toInt();

    qDebug()<<"R:"<<action->text();
    cl->resize(W,H);
    imgvis->resize_image(W,H);

}

void mainW::menu_set_levels(QAction*action){
    qDebug()<<"L:"<<action->text();

    int L=action->text().toInt();
    imgvis->update_col_levels(L);
}
