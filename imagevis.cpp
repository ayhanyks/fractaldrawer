#include "imagevis.h"
#include "QDebug"
#include "math.h"
void colmap(WORKLOAD*W);

imageVis::imageVis(QObject *parent) : QObject(parent)
{
    pandxtotal = 0;
    pandytotal = 0;

    out_buf_size = 0;

    clabel=new clickLabel((QWidget*)parent);
    clabel->setAutoFillBackground(true);
    clabel->show();
    connect(this,SIGNAL(updateImg(quint8*)), clabel,SLOT(updateImg(quint8*)));

    connect(clabel, SIGNAL(pan(int,int)), this,SLOT(pan(int,int)));
    connect(clabel,SIGNAL(zoomIn(int,int)),this,SLOT(zoomIn(int,int)));
    connect(clabel,SIGNAL(zoomOut(int,int)),this,SLOT(zoomOut(int,int)));
    connect(clabel,SIGNAL(mousehover(int,int)),this,SLOT(mousehover(int,int)));
}

void imageVis::openFile(QFile&f){

    f.read((char*)&Window->xc,sizeof(Window->xc) );
    f.read((char*)&Window->yc,sizeof(Window->yc) );
    f.read((char*)&Window->zoom,sizeof(Window->zoom) );
    f.read((char*)&Window->numlevels,sizeof(Window->numlevels));
    f.read((char*)Window->colMap,sizeof(uint32_t)*Window->numlevels);


    fpu_startWorkload(Window);
    colmap(Window);
    updateHistogram(Window);
    emit updateImg((quint8*)Window->rgbBuf);
    emit updateHistogramSignal(Window->histogram,Window->numlevels );


}

void imageVis::mousehover(int x, int y){

   double realx, realy;

   long double ar=1.0*Window->W/Window->H;

   long double height=pow(2,Window->zoom);
   long double width=pow(2,Window->zoom)*ar;

   long double ybtm = Window->yc  - (height/2.0);
   long double xleft = Window->xc - (width/2.0);

   realx = xleft + (double)width*x/Window->W;
   realy = ybtm + (double)height*(Window->H-y)/Window->H;

   QString S;
   S=QString("X:%1 Y:%2 | ").arg(realx,8,'f',6).arg(realy,8,'f',6);

   uint32_t out_level = outputBuf[current_buf_id][x+y*Window->W];
   uint8_t cr = PIXBUFF[current_buf_id][(x+y*Window->W)*3];
   uint8_t cg = PIXBUFF[current_buf_id][(x+y*Window->W)*3+1];
   uint8_t cb = PIXBUFF[current_buf_id][(x+y*Window->W)*3+2];

   S.append(QString("LVL: %1 ").arg(outputBuf[current_buf_id][x+y*Window->W]&0xFFFF));
   S.append(QString("S: %1 ").arg(outputBuf[current_buf_id][x+y*Window->W]>>16));
   S.append(QString("RGB: %1/%2/%3").arg(cr).arg(cg).arg(cb));

   S.append(QString("| Z:%1").arg(Window->zoom));

 //  qDebug()<<"height"<<(double)height;
 //  qDebug()<<"width"<<(double)width;

 //  qDebug()<<"ybtm"<<(double)ybtm;
 //  qDebug()<<"xleft"<<(double)xleft;

 //  qDebug()<<"realx"<<(double)realx;
 //  qDebug()<<"realy"<<(double)realy;


   emit statusMsg(S);

}

void imageVis::saveFile(QFile&f){

    //QFile f(name);
    //f.open(QIODevice::WriteOnly);
    f.write((const char*)&Window->xc,sizeof(Window->xc));
    f.write((const char*)&Window->yc,sizeof(Window->yc));
    f.write((const char*)&Window->zoom,sizeof(Window->zoom));
    f.write((const char*)&Window->numlevels,sizeof(Window->numlevels));
    f.write((const char*)Window->colMap,sizeof(uint32_t)*Window->numlevels);
   // f.close();
}



void imageVis::resize_image(int W,int H){

    out_buf_size = (W)*(H)*sizeof(uint32_t);
    pix_buf_size = (W)*(H)*sizeof(quint8)*3;
    for(int bid=0;bid<NUMBUF;bid++)
    {
        free((void*)outputBuf[bid]);
        free((void*)outputBuf2[bid]);
        free((void*)PIXBUFF[bid]);

    }

    for(int bid=0;bid<NUMBUF;bid++)
    {
        outputBuf[bid] = (uint32_t*)malloc(out_buf_size);
        outputBuf2[bid] = (uint32_t*)malloc(out_buf_size);
        PIXBUFF[bid] = (quint8*)malloc(pix_buf_size);
    }
    current_buf_id=0;
    clabel->resize(W,H);



    Window->H = H;
    Window->W = W;
    Window->xstart = 0;
    Window->xend = W;
    Window->ystart = 0;
    Window->yend = H;

    Window->outputBuf =outputBuf[current_buf_id];
    Window->outputBuf2 =outputBuf2[current_buf_id];
    Window->rgbBuf = PIXBUFF[current_buf_id];

    fpu_startWorkload(Window);
    colmap(Window);
    updateHistogram(Window);

    emit updateImg((quint8*)Window->rgbBuf);
    emit updateHistogramSignal(Window->histogram,Window->numlevels );



}

void imageVis::update_col_levels(int colMapSize)
{
    Window->numlevels=colMapSize;
    delete Window->histogram;
    Window->histogram=new quint32[colMapSize];

    delete Window->colMap;
    colorMap=new quint32[colMapSize];

//    for(int i=0;i<colMapSize;i++)
//    {
//        colorMap[i] = (((i*256)/colMapSize)<<16) | (((i*256)/colMapSize)<<8) | ((i*256)/colMapSize);
//    }

    Window->colMap = colorMap;


    fpu_startWorkload(Window);
    colmap(Window);

    updateHistogram(Window);

    emit updateImg((quint8*)Window->rgbBuf);

    updateHistogram(Window);

    emit updateImg((quint8*)Window->rgbBuf);
    //printf("pix buff:0x%08x\n",Window->rgbBuf);
    //printf("Window->numlevels:0x%08x\n",Window->numlevels);
    //printf("pix buff:0x%08x\n",((quint8*)Window->rgbBuf)[100]);



}

void imageVis::setup(quint32 W, quint32 H,int colMapSize){

    out_buf_size = (W)*(H)*sizeof(uint32_t);
    pix_buf_size = (W)*(H)*sizeof(quint8)*3;
    for(int bid=0;bid<NUMBUF;bid++)
    {
        outputBuf[bid] = (uint32_t*)malloc(out_buf_size);
        outputBuf2[bid] = (uint32_t*)malloc(out_buf_size);
        PIXBUFF[bid] = (quint8*)malloc(pix_buf_size);
    }
    current_buf_id=0;
    clabel->resize(W,H);

    colorMap=new quint32[colMapSize];

    for(int i=0;i<colMapSize;i++)
    {
        colorMap[i] = (((i*256)/colMapSize)<<16) | (((i*256)/colMapSize)<<8) | ((i*256)/colMapSize);
    }

    Window = (WORKLOAD*)malloc(sizeof(WORKLOAD));

    Window->done = false;
    Window->H = H;
    Window->W = W;

    Window->xc = 0;
    Window->yc = 0;
    Window->zoom = 2;
    Window->xstart = 0;
    Window->xend = W;
    Window->ystart = 0;
    Window->yend = H;
    Window->outputBuf =outputBuf[current_buf_id];
    Window->outputBuf2 =outputBuf2[current_buf_id];
    Window->rgbBuf = PIXBUFF[current_buf_id];
    Window->histogram=new quint32[colMapSize];
    Window->colMap = colorMap;
    Window->callback=colmap;
    Window->numlevels=colMapSize;
    Window->hist_eq = false;
    fpu_startWorkload(Window);
    colmap(Window);
    updateHistogram(Window);

    emit updateImg((quint8*)Window->rgbBuf);
    //printf("pix buff:0x%08x\n",Window->rgbBuf);
    //printf("Window->numlevels:0x%08x\n",Window->numlevels);

    //printf("pix buff:0x%08x\n",((quint8*)Window->rgbBuf)[100]);

}


void updateHistogram(WORKLOAD*W)
{
    //histogram equalization..



    int w,h;
    quint32 dataindex=0;
    memset(W->histogram,0,sizeof(quint32)*W->numlevels);
   // qDebug()<<"update histogram"<<W->histogram[0];
    for(w=0;w<W->W;w++)
    {
        for(h=0;h<W->H;h++)
        {
            W->histogram[W->outputBuf[dataindex++]&0xFFFF]++;
        }
    }



}

void colmap(WORKLOAD*W){

    //printf("ppe callback 0x%08x\n",W);
    uint32_t x,y;
    uint32_t pbx;
    uint32_t pvx;


    uint32_t*obuf=W->outputBuf;
    uint32_t*obuf2=W->outputBuf2;
    quint8*pbuf=(quint8*)W->rgbBuf;

    int maxtone=0;
    int mintone=999999;
    int tone;
    //qDebug()<<"W->xstart"<<W->xstart<<"W->xend"<<W->xend;
    //qDebug()<<"W->ystart"<<W->ystart<<"W->yend"<<W->yend;


    quint32 dataindex = 0;
    if(W->hist_eq && false)
    {
        qDebug()<<"hist eq";
        updateHistogram(W);

        for(int w=0;w<W->W;w++)
        {
            for(int h=0;h<W->H;h++)
            {

                quint32 val = obuf[dataindex]&0xFFFF;

                quint32 htot = 0;
                for(int l = 0; l <= val; l++)
                {
                    htot += W->histogram[l];
                }

                obuf2[dataindex] = (W->numlevels-1)*(1.0*(double)htot/(double)((W->W)*(W->H)));

                if (obuf2[dataindex]>=W->numlevels)
                        obuf2[dataindex] = W->numlevels - 1;
                dataindex++;
            }

        }
    }
    else
    {
        dataindex=0;
        for(int w=0;w<W->W;w++)
        {
            for(int h=0;h<W->H;h++)
            {
                quint32 val = obuf[dataindex]&0xFFFF;
                obuf2[dataindex] = val;
                dataindex++;

                if(val>maxtone)
                    maxtone=val;
                if(val<mintone)
                    mintone=val;
            }

        }
    }


    for(y=W->ystart;y<W->yend;y++)
    {

        pbx = (W->W*y+W->xstart)*3;
        pvx = (W->W*y+W->xstart);

        for(x=W->xstart;x<W->xend;x++){
            quint32 oval = obuf2[pvx]&0xFFFF;
            uint8_t itr_lvl = obuf[pvx] >> 16;

            //quint32 oval = oval0;
            if(W->hist_eq)
                oval =   maxtone*(oval-mintone)/(maxtone-mintone);

            quint32 pixc = W->colMap[oval];


           // if((pixc&0xFF)!=((pixc>>8)&0xFF))
           //     qDebug()<<oval0<<oval<<hex<<pixc<<maxtone<<mintone;

            uint8_t r,g,b,r0,g0,b0,r1,g1,b1;

            r = (W->colMap[oval] & 0x0000FF);
            g = (W->colMap[oval] & 0x00FF00)>> 8;
            b = (W->colMap[oval] & 0xFF0000)>> 16;

            if(oval){
                r0 = (W->colMap[oval-1] & 0x0000FF);
                g0 = (W->colMap[oval-1] & 0x00FF00)>> 8;
                b0 = (W->colMap[oval-1] & 0xFF0000)>> 16;
            }

            if(oval<(W->numlevels-1)){
                r1 = (W->colMap[oval+1] & 0x0000FF);
                g1 = (W->colMap[oval+1] & 0x00FF00)>> 8;
                b1 = (W->colMap[oval+1] & 0xFF0000)>> 16;
            }

            if(itr_lvl<=128)
            {
                r = r0-(r0-r)*itr_lvl/128;
                g = g0-(g0-g)*itr_lvl/128;
                b = b0-(b0-b)*itr_lvl/128;
            }
            else
            {
                r = r-((r-r1)*(itr_lvl-128))/127;
                g = g-((g-g1)*(itr_lvl-128))/127;
                b = b-((b-b1)*(itr_lvl-128))/127;
            }



            pbuf[pbx++]=itr_lvl;
            pbuf[pbx++]=itr_lvl;
            pbuf[pbx++]=itr_lvl;
            pvx++;

        }
    }


}


void imageVis::pan(int dx, int dy){

//pan the image.
//we can still have some part of the image inside. so no need to recalculate
//but we need to copy the part into new location..
    qDebug()<<"imageVis::pan"<<dx<<dy;

    //dx = -dx;
    //dy = -dy;

    double ddx;
    double ddy;

    uint32_t nextbufid = (current_buf_id+1)%NUMBUF;

    //memcpy(outputBuf[nextbufid], outputBuf[current_buf_id],out_buf_size);

    Window->outputBuf=outputBuf[nextbufid];
    Window->rgbBuf=PIXBUFF[nextbufid];

    uint32_t wlen=Window->xend-Window->xstart;
    uint32_t whgh=Window->yend-Window->ystart;

    uint32_t pbufwlen=wlen*3*sizeof(quint8);
    uint32_t obufwlen = wlen*sizeof(uint32_t);
    uint32_t pbufwhgh=whgh*3*sizeof(quint8);

    WORKLOAD*SubWindow=new WORKLOAD;

    WORKLOAD SubWindow1, SubWindow2;

    if(dx || dy)
    {


        Window->xc-= pow(2,Window->zoom)*(double)dx/(double)Window->H;
        Window->yc-= pow(2,Window->zoom)*(double)dy/(double)Window->H;
        //memcpy(SubWindow,Window,sizeof(WORKLOAD));

        uint32_t srcx,srcy,dstx,dsty;
        uint32_t cpyw,cpyh;

        uint32_t clcx,clcy;
        uint32_t clcw,clch;

        dstx = dx>0?dx:0;
        srcx = dx>0?0:(-dx);

        dsty = dy>0?dy:0;
        srcy = dy>0?0:(-dy);

        cpyw = dx>0?wlen-dx:wlen+dx;
        cpyh = dy>0?whgh-dy:whgh+dy;

        qDebug()<<"DSTSRC:"<<dstx<<dsty<<srcx<<srcy<<cpyw<<cpyh;

        memset(PIXBUFF[nextbufid],0x00,wlen*whgh*3);
        memset(outputBuf[nextbufid],0x00,wlen*whgh*4);

        for(unsigned int y=0; y<cpyh;y++)
        {

            memcpy((void*)((size_t)PIXBUFF[nextbufid]+(y+dsty)*pbufwlen+dstx*3), (void*)((size_t)PIXBUFF[current_buf_id]+(y+srcy)*pbufwlen+srcx*3), cpyw*3);
            memcpy((void*)((size_t)outputBuf[nextbufid]+(y+dsty)*obufwlen+dstx*4), (void*)((size_t)outputBuf[current_buf_id]+(y+srcy)*obufwlen+srcx*4), cpyw*4);

        }

        SubWindow1 = *Window;
        SubWindow2 = *Window;

        clcx = dx>0?0:wlen+dx;
        clcw = dx>0?dx:(-dx);
        clcy = 0;
        clch = whgh;


        SubWindow1.xstart = clcx;
        SubWindow1.xend = clcx + clcw;
        SubWindow1.ystart = 0;
        SubWindow1.yend = whgh;

        qDebug()<<"SW1:"<<clcx<<clcy<<clcw<<clch;

        if(dx)
            fpu_startWorkload(&SubWindow1);


        clcy = dy>0?0:whgh+dy;
        clch = dy>0?dy:(-dy);
        clcx = dx>0?dx:0;
        clcw = wlen-clcx;

        SubWindow2.xstart = clcx;
        SubWindow2.xend = clcx + clcw;
        SubWindow2.ystart = clcy;
        SubWindow2.yend = clch+clcy;

        qDebug()<<clcx<<clcy<<clcw<<clch;
        if(dy)
            fpu_startWorkload(&SubWindow2);

        colmap(Window);
        current_buf_id = nextbufid;

        updateHistogram(Window);
        //emit updateLocation();

        //1. copy the sub window
        emit updateImg((quint8*)Window->rgbBuf);
        emit updateHistogramSignal(Window->histogram, Window->numlevels);

    }
    return;
    if(dx)
    {
        Window->xc+= pow(2,Window->zoom)*(double)dx/(double)Window->W;
        memcpy(SubWindow,Window,sizeof(WORKLOAD));
        if(dx>0){

            for(unsigned int y=Window->ystart; y<Window->yend;y++)
            {
                memcpy((void*)((size_t)PIXBUFF[nextbufid]+y*pbufwlen), (void*)((size_t)PIXBUFF[current_buf_id]+y*pbufwlen+dx*3), pbufwlen-dx*3);
                memcpy((void*)((size_t)outputBuf[nextbufid]+y*obufwlen), (void*)((size_t)outputBuf[current_buf_id]+y*obufwlen+dx*4), obufwlen-dx*4);

            }

            SubWindow->xstart = Window->xend  - dx;
            SubWindow->xend = Window->xend;

            fpu_startWorkload(SubWindow);
        }
        else
        {
            dx=-dx;
            for(unsigned int y=Window->ystart; y<Window->yend;y++)
            {
                memcpy((void*)((size_t)PIXBUFF[nextbufid]+y*pbufwlen+dx*3), (void*)((size_t)PIXBUFF[current_buf_id]+y*pbufwlen), pbufwlen-dx*3);
                memcpy((void*)((size_t)outputBuf[nextbufid]+y*obufwlen+dx*4), (void*)((size_t)outputBuf[current_buf_id]+y*obufwlen), obufwlen-dx*4);

            }
            //memcpy((void*)((size_t)PIXBUFF[nextbufid]+dx*3), PIXBUFF[current_buf_id], pix_buf_size-dx*3);

            SubWindow->xstart = 0;
            SubWindow->xend = dx;

            fpu_startWorkload(SubWindow);


        }
    }

    if(dy)
    {
        Window->yc+= pow(2,Window->zoom)*dy/(float)Window->H;
        memcpy(SubWindow,Window,sizeof(WORKLOAD));

        if(dy>0)
        {
           // memcpy(outputBuf[nextbufid], (void*)((size_t)outputBuf[current_buf_id]+dy*wlen),out_buf_size-dy*wlen);
            memcpy(PIXBUFF[nextbufid], (void*)((size_t)PIXBUFF[current_buf_id]+dy*pbufwlen), pix_buf_size-dy*pbufwlen);
            memcpy(outputBuf[nextbufid], (void*)((size_t)outputBuf[current_buf_id]+dy*obufwlen), out_buf_size-dy*obufwlen);

            SubWindow->ystart = Window->yend - dy;
            SubWindow->yend = Window->yend;
            //SubWindow->xstart = Window->xstart;
            //SubWindow->xend = Window->xend;
            fpu_startWorkload(SubWindow);

        }
        else
        {
            dy=-dy;
            //memcpy((void*)((size_t)outputBuf[nextbufid]+dy*wlen),outputBuf[current_buf_id],out_buf_size-dy*wlen);
            memcpy((void*)((size_t)PIXBUFF[nextbufid]+dy*pbufwlen), PIXBUFF[current_buf_id], pix_buf_size-dy*pbufwlen);
            memcpy((void*)((size_t)outputBuf[nextbufid]+dy*obufwlen), outputBuf[current_buf_id], out_buf_size-dy*obufwlen);

            SubWindow->ystart = 0;
            SubWindow->yend = dy;
            //SubWindow->xstart = Window->xstart;
            //SubWindow->xend = Window->xend;
            fpu_startWorkload(SubWindow);

        }

        //
    }


    if (dy || dx){
        current_buf_id = nextbufid;
        updateHistogram(Window);
        emit updateImg((quint8*)Window->rgbBuf);
    }



    //startWorkload(Window);


}

void imageVis::zoomOut(int cx, int cy){
    //select new center


    Window->xc -= pow(2,Window->zoom)*(((double)cx/(double)Window->W)-0.5);
    Window->yc -= pow(2,Window->zoom)*(((double)cy/(double)Window->H)-0.5);
    Window->zoom ++ ;

    qDebug()<<"zoom:"<<Window->zoom;

    fpu_startWorkload(Window);
    colmap(Window);
    updateHistogram(Window);
    emit updateImg((quint8*)Window->rgbBuf);
    emit updateHistogramSignal(Window->histogram, Window->numlevels);

}

void imageVis::zoomIn(int cx, int cy){
    //select new center

    Window->zoom -- ;

    qDebug()<<"zoom:"<<Window->zoom;

    Window->xc += pow(2,Window->zoom)*(((double)cx/(double)Window->W)-0.5);
    Window->yc += pow(2,Window->zoom)*(((double)cy/(double)Window->H)-0.5);

    fpu_startWorkload(Window);
    colmap(Window);
    updateHistogram(Window);
    emit updateImg((quint8*)Window->rgbBuf);
    emit updateHistogramSignal(Window->histogram, Window->numlevels);

}

void imageVis::draw(){
    int x,y;

    myPix = new QPixmap(Window->W,Window->H);
    QPainter *painter = new QPainter(myPix);
    QPen paintpen(Qt::red);
    paintpen.setWidth(1);
    int tone;

    int maxtone=0;
    int mintone=999999;

    for(x=0;x<Window->W;x++)
    {
        for(y=0;y<Window->H;y++)
        {
            tone=IMGDATA[x*Window->H+y];
            if(tone>maxtone)
                maxtone=tone;
            if(tone<mintone)
                mintone=tone;
        }
    }
    printf("max tone:%d mintone=%d\n",maxtone,mintone);

    for(x=0;x<Window->W;x++)
    {
        for(y=0;y<Window->H;y++)
        {
                tone=IMGDATA[x*Window->H+y];
                //tone=65535-tone;
                //tone=(255*(tone-mintone))/(maxtone-mintone);
                //tone=255-tone;
                //qDebug()<<tone;

                paintpen.setColor(QColor(QRgb(colorMap[tone])));
                painter->setPen(paintpen);
                painter->drawPoint(QPoint(x,y));
        }
    }
    myLabel->setPixmap(myPix->scaled(myLabel->width(),myLabel->height()));

    pandxtotal = 0;
    pandytotal = 0;

}



void imageVis::updateColorMap(quint32 *colMap, quint32 numcolors, bool hist_eq){

    if(Window->numlevels!=numcolors)
        update_col_levels(numcolors);

    qDebug()<<"updating color map.."<<colMap[0];
    for(int i=0;i<numcolors;i++)
    {
        colorMap[i] = colMap[i];
    }
    Window->hist_eq=hist_eq;
    colmap(Window);
    updateHistogram(Window);

    emit updateImg((quint8*)Window->rgbBuf);
    emit updateHistogramSignal(Window->histogram, Window->numlevels);
}
