#ifndef IMAGEVIS_H
#define IMAGEVIS_H

#include <QObject>
#include <QLabel>
#include <QPainter>
#include <QPixmap>
#include "fpu.h"
#include "clicklabel.h"

#define NUMBUF  4

void updateHistogram(WORKLOAD*W);
void colmap(WORKLOAD*W);
class imageVis : public QObject
{
    Q_OBJECT
public:
    explicit imageVis(QObject *parent = nullptr);

    void setup(quint32 W, quint32 H,int colMapSize);
    void update_col_levels(int colMapSize);

    QLabel*myLabel;
    int*IMGDATA;
    QPixmap *myPix;
    int padX;
    int padY;
    int pandxtotal;
    int pandytotal;
    void draw();
    int getW(){
        return Window->W;
    }
    int getH(){
        return Window->H;
    }
    quint32*getHistogram()
    {
        return Window->histogram;
    }
    quint32 histogramGetLevels(){
        return Window->numlevels;
    }
    void set_histogram_equalize(bool enable){
        Window->hist_eq = enable;
        fpu_startWorkload(Window);
        colmap(Window);
        emit updateImg((quint8*)Window->rgbBuf);
    }

    double xc(){return Window->xc;}
    double yc(){return Window->yc;}
    double zl(){return Window->zoom;}
    QWidget*getScren(){
        return clabel;
    }

public slots:
    void pan(int dx, int dy);
    void zoomIn(int cx, int cy);
    void zoomOut(int cx, int cy);
    void updateColorMap(quint32*colMap, quint32 numcolors, bool hist_eq);
    void resize_image(int W,int H);
    void saveFile(QFile &f);
    void openFile(QFile &f);
private:
    quint32*outputBuf[NUMBUF];
    quint32*outputBuf2[NUMBUF];
    quint8*PIXBUFF[NUMBUF];
    quint32*colorMap;
    quint32 colMapSize;
    quint32 out_buf_size;
    quint32 pix_buf_size;
    WORKLOAD*Window;
    quint32 current_buf_id;
    clickLabel*clabel;

private slots:
    void mousehover(int,int);
signals:
    void updateImg(quint8*DATABUFF);
    void updateData(quint32*ImgBuff);
    void updateLocation();
    void statusMsg(QString S);
    void updateHistogramSignal(quint32*,quint32);
};

#endif // IMAGEVIS_H
