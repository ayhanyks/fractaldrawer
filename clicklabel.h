#ifndef CLICKLABEL_H
#define CLICKLABEL_H
#include <QLabel>
#include <QMouseEvent>
#include <QPainter>


class clickLabel : public QLabel
{
    Q_OBJECT
public:
    explicit clickLabel(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~clickLabel();


public slots:
    void updateImg(quint8*imgbuff);
    void updateHistogram(quint8 *imgbuf);

signals:
    void leftclick(int x, int y);
    void rightclick(int x, int y);
    void zoomRecSelected(QRect rec);
    void mouseMove(int dx, int dy);
    void pan(int dx, int dy);
    void zoomIn(int cx, int cy);
    void zoomOut(int cx, int cy);
    void resized();
    void mousehover(int,int);
private:
    int gestureMode;
    int zoomrecx0;
    int zoomrecx1;
    int zoomrecy0;
    int zoomrecy1;
     QPainter *painter;
    //int max(int a,int b){return(a>b?a:b);}
     int total_panx,total_pany;
     QPixmap refpixmap;

     int oldx;
     int oldy;
     bool oldxyValid;

     quint8*imgdata;
     QImage img;
protected:
    void mouseDoubleClickEvent(QMouseEvent*e);
    void mouseMoveEvent(QMouseEvent*e);
    void mouseReleaseEvent(QMouseEvent*e);
    void mousePressEvent(QMouseEvent*e);
    void resizeEvent(QResizeEvent *event);
};

#endif // CLICKLABEL_H
