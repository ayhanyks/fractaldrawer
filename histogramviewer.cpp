#include "histogramviewer.h"
#include "QDialog"
#include "QHBoxLayout"
#include "QDebug"
#include "QRgb"
#include "QColor"

#define DEFAULT_SIZE_X  256
#define DEFAULT_SIZE_Y  64

histogramViewer::histogramViewer(imageVis *imgvis, QObject *parent) : QObject(parent)
{
    this->imgvis=imgvis;
    QDialog*qdlg=new QDialog((QWidget*)parent);

    qdlg->resize(DEFAULT_SIZE_X,DEFAULT_SIZE_Y);
    qdlg->setWindowTitle("histogram");
    histlb=new clickLabel(qdlg);
    histlb->setStyleSheet("background-color: rgb(0, 215, 207);");
    histlb->setMinimumSize(1,1);

    QHBoxLayout *hlayout = new QHBoxLayout(qdlg);
    //hlayout->setMargin(0);
    hlayout->addWidget(histlb);

    connect(imgvis,SIGNAL(updateImg(quint8*)),this,SLOT(updateHistogram(quint8*)));

    qdlg->show();

    updateHistogram(nullptr);

    connect(histlb,SIGNAL(resized()),this,SLOT(updateHistogram()));
}




void histogramViewer::updateHistogram(){
    //qDebug()<<"histogram update received";

    qDebug()<<"update histogram...";
    //imageVis*imgvis=(imageVis*)sender();

    QPixmap pix;
    pix = QPixmap(histlb->width(), histlb->height());
    pix.fill( Qt::white );

    QPainter paint(&pix);

    quint32*hist=imgvis->getHistogram();

    paint.setBrush(QBrush(QColor(QRgb(0))));

    int max_hist_level = 0;
    for(int x=0;x<imgvis->histogramGetLevels();x++)
    {
        qDebug()<<"hist"<<x<<hist[x];

        if(max_hist_level<hist[x])
            max_hist_level=hist[x];
    }
    for(int x=0;x<imgvis->histogramGetLevels();x++)
    {
        int recxstart = x*histlb->width()/imgvis->histogramGetLevels();
        //int recxend = (x+1)*histlb->width()/imgvis->histogramGetLevels();
        int recystart = histlb->height() - ((histlb->height()*hist[x]) / max_hist_level);
        //int recyend = histlb->height() - ((histlb->height()*hist[x]) / max_hist_level);
        qDebug()<<recxstart<<((histlb->height()*hist[x]) / max_hist_level);
        paint.drawRect(recxstart, recystart ,histlb->width()/imgvis->histogramGetLevels(),((histlb->height()*hist[x]) / max_hist_level));

    }


    histlb->setPixmap(pix);
}

void histogramViewer::updateHistogram(quint8*imgbuff){
    updateHistogram();
}
