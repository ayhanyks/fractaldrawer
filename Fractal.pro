#-------------------------------------------------
#
# Project created by QtCreator 2020-01-26T22:54:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Fractal
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp\
    imagevis.cpp \
   # mainwindow.cpp \
    clicklabel.cpp \
    #mandelbrotdrawer.cpp \
    fpu.cpp \
    tonemapper.cpp \
    clabel.cpp \
    histogramviewer.cpp \
    colormap.cpp \
    control_panel.cpp \
    size_level.cpp \
    mainw.cpp \
    mainwindow.cpp

HEADERS  += clicklabel.h \
    imagevis.h \
  #  ui_mainwindow.h \
    #mandelbrotdrawer.h \
    fpu.h \
    tonemapper.h \
    clabel.h \
    histogramviewer.h \
    colormap.h \
    control_panel.h \
    size_level.h \
    mainw.h \
    mainwindow.h

FORMS    += tonemapper.ui \
    histogramview.ui \
    colormap.ui \
    control_panel.ui \
    size_level.ui \
    mainwindow.ui

LIBS +=-lmpfr -lgmp
