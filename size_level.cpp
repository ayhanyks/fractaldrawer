#include "size_level.h"
#include "ui_size_level.h"
#include <QDebug>

static const int levels[]={16,32,64,128,256,512,1024,2048};

size_level::size_level(imageVis *imgvis, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::size_level)
{
    this->imgvis=imgvis;
    ui->setupUi(this);


    for(int i=0;i<sizeof(levels)/sizeof(levels[0]);i++)
    {
        ui->comboBox_level_selection->addItem(QString::number(levels[i]));
    }
    ui->comboBox_level_selection->addItem("other");



    //get slider value
    qDebug()<<"imgvis->histogramGetLevels()"<<imgvis->histogramGetLevels();
   // ui->horizontalSlider->setValue(log2x(imgvis->histogramGetLevels()));


}

size_level::~size_level()
{
    delete ui;
}

void size_level::on_pushButton_clicked()
{
    //update size of image

}

void size_level::on_horizontalSlider_valueChanged(int value)
{
    //update number of levels (by two?)
    uint32_t valx=(1<<value);
    if(imgvis->histogramGetLevels()!=valx){
        imgvis->update_col_levels(valx);

    }
    //ui->label_3->setText(QString::number(valx));
}
