#include "colormap.h"
#include "ui_colormap.h"
#include "QPainter"
#include "QDebug"
#include "clicklabel.h"

colormap::colormap(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::colormap)
{
    ui->setupUi(this);


    cmap.num_levels = 0;
    cmap.allCols=new quint32[8192];

    cmap.keyCols[Col::R].append(key_color{0.0,0.0});
    cmap.keyCols[Col::R].append(key_color{1.0,1.0});

    cmap.keyCols[Col::G].append(key_color{0.0,0.0});
    cmap.keyCols[Col::G].append(key_color{1.0,1.0});

    cmap.keyCols[Col::B].append(key_color{0.0,0.0});
    cmap.keyCols[Col::B].append(key_color{1.0,1.0});



    ui->rgbset->setcorners(-0.1,-0.1,1.1,1.1);
    connect(ui->rgbset,SIGNAL(touchEvent(double,double)),this,SLOT(rgbset_touch(double,double)));
    connect(ui->rgbset,SIGNAL(moveEvent(double,double)), this,SLOT(rgbset_move(double,double)));
    connect(ui->rgbset,SIGNAL(untouchEvent(double,double)), this,SLOT(rgbset_untouch(double,double)));

    selected_keycolor_index = -1;
    selected_keycolor_color = -1;
}

colormap::~colormap()
{
    delete ui;
}



void colormap::saveFile(QFile &f){
    f.write((const char*)&cmap.num_cols,sizeof(cmap.num_cols));
    f.write((const char*)&cmap.num_levels,sizeof(cmap.num_levels));
    f.write((const char*)cmap.allCols,sizeof(uint32_t)*cmap.num_levels);

    for(int c=0; c<3;c++)
    {
        uint32_t len = cmap.keyCols[c].length();
        f.write((const char*)&len,sizeof(len));
        for(int i=0; i<(cmap.keyCols[c].length());i++)
            f.write((const char*)&cmap.keyCols[c][i],sizeof(cmap.keyCols[c][i]));
    }
}

void colormap::loadFile(QFile &f){
    f.read((char*)&cmap.num_cols,sizeof(cmap.num_cols));
    f.read((char*)&cmap.num_levels,sizeof(cmap.num_levels));
    f.read((char*)cmap.allCols,sizeof(uint32_t)*cmap.num_levels);


    for(int c=0; c<3;c++)
    {
        uint32_t len;
        f.read((char*)&len, sizeof(len));

        cmap.keyCols[c].clear();
        for(int i=0; i<len;i++)
        {
            key_color kc;
            f.read((char*)&kc, sizeof(key_color));
            cmap.keyCols[c].append(kc);
        }
    }

    ui->horizontalSlider->setSliderPosition(cmap.num_cols);
    update_color_map();

}


void colormap::rgbset_move(double x, double y){
    qDebug()<<"rgbset_move"<<x<<y;

    if(selected_keycolor_index>=0)
    {

        //<<<
        if(x<cmap.keyCols[selected_keycolor_color][selected_keycolor_index].x)
        {
            if(selected_keycolor_index>0)
            {
                if(cmap.keyCols[selected_keycolor_color][selected_keycolor_index-1].x < x)
                    cmap.keyCols[selected_keycolor_color][selected_keycolor_index].x = x;
            }
            else
            {
                if(x>=0)
                    cmap.keyCols[selected_keycolor_color][selected_keycolor_index].x = x;
            }
        }
        else if(x>cmap.keyCols[selected_keycolor_color][selected_keycolor_index].x)
        {
            if(selected_keycolor_index<(cmap.keyCols[selected_keycolor_color].length()-1))
            {
                if(cmap.keyCols[selected_keycolor_color][selected_keycolor_index+1].x > x)
                    cmap.keyCols[selected_keycolor_color][selected_keycolor_index].x = x;
            }
            else
            {
                if(x<=1)
                    cmap.keyCols[selected_keycolor_color][selected_keycolor_index].x = x;
            }
        }

        y = (y<0)?0:(y>1)?1:y;

        cmap.keyCols[selected_keycolor_color][selected_keycolor_index].y=y;




        update_color_map();
    }
}

void colormap::rgbset_untouch(double x, double y){
    selected_keycolor_index=-1;
}

void colormap::rgbset_touch(double x, double y){
    qDebug()<<"rgbset_touch"<<x<<y;

    selected_keycolor_color=-1;
    for(int c=0; c<3;c++)
    {
        if(c==Col::R && ui->checkBox_R->isChecked()==false)
        {
            continue;
        }
        else if(c==Col::G && ~ui->checkBox_G->isChecked()==false)
        {
            continue;
        }
        else if(c==Col::B && ~ui->checkBox_B->isChecked()==false)
        {
            continue;
        }

        for(int i=0; i<cmap.keyCols[c].length();i++)
        {

            double cx = cmap.keyCols[c][i].x;
            double cy = cmap.keyCols[c][i].y;

            double xdiff = x>cx?(x-cx):(cx-x);
            double ydiff = y>cy?(y-cy):(cy-y);

            double dist2= (xdiff*xdiff)+(ydiff*ydiff);


            qDebug()<<"dist2"<<dist2;


            if(dist2<0.0001)
            {
                qDebug()<<"rgbset_touch red key"<<dist2;
                selected_keycolor_index = i;
                selected_keycolor_color = c;
/*
                if(selected_keycolor_index==0)
                {
                    cmap.keyCols[c].insert(1,key_color{cx,cy});
                    selected_keycolor_index=1;
                }
*/
                break;
            }
        }
        if(selected_keycolor_color>=0)
            break;
    }

    if(selected_keycolor_color<0)
    {
        //add a point if over line
        for(int c=0; c<3;c++)
        {
            for(int i=0; i<(cmap.keyCols[c].length()-1);i++)
            {
                key_color kc0 = cmap.keyCols[c][i];
                key_color kc1 = cmap.keyCols[c][i+1];

                if(x<kc0.x || x>kc1.x)
                    continue;

                double m0 = (kc1.y-kc0.y)/(kc1.x-kc0.x);
                double m1 = (y-kc0.y)/(x-kc0.x);


                qDebug()<<"m0m1"<<m0<<m1;
                if(((m0-m1)*(m0-m1))<0.01)
                {
                    cmap.keyCols[c].insert(i+1,key_color{x,y});
                    selected_keycolor_index = i+1;
                    selected_keycolor_color = c;
                    break;
                }
            }
            if(selected_keycolor_color>=0)
                break;
        }
    }


}

void colormap::resizeEvent(QResizeEvent *event)
{
    if (!cmap.num_levels)
        return;

    update_color_map();
}


void colormap::set_num_levels(int numl){

   // if (cmap.num_levels)
   //     delete cmap.allCols;


    cmap.num_levels = numl;
    cmap.num_cols = cmap.num_levels;

   // cmap.allCols = new quint32[numl];
    update_color_map();
    qDebug()<<"color_map_update_event"<<numl;
    emit color_map_update_event(cmap.allCols, numl,ui->equlize->isChecked());
    ui->horizontalSlider->setMaximum(cmap.num_levels);
    ui->horizontalSlider->setValue(cmap.num_cols);

    if(ui->num_levels->value()!=cmap.num_levels)
        ui->num_levels->setValue(cmap.num_levels);
}



void colormap::create_mid_colors(){

    qDebug()<<"create_mid_colors";
    int ckeyc = 0;

    for(int c=0;c<cmap.num_levels;c++)
    {
        cmap.allCols[c] =0;
    }

    int col_left;

    for(int c=0;c<3;c++)
    {
        col_left=cmap.num_levels;
        int numc = 0;

        while (col_left)
        {
            for (int x=0;x<cmap.num_cols;x++)
            {

                float xf = 1.0*x/(cmap.num_cols - 1);

                //find an interval..
                qint8 col;

                for(int i=0;i<cmap.keyCols[c].length()-1;i++)
                {

                    key_color c0 = cmap.keyCols[c][i];
                    key_color cn = cmap.keyCols[c][i+1];
                    double x0 = c0.x;
                    double x1 = cmap.keyCols[c][i+1].x;

                    if(xf<x0)
                    {
                        col = int(c0.y*255);
                        //qDebug()<<"xf<x0";
                        break;
                    }

                    if(xf>x1)
                    {
                        //qDebug()<<"xf>x0";
                        continue;
                        break;
                    }



                    double dx = x1 - x0;
                    double dx0 = xf - x0;


                    col=int(255*( c0.y+(cn.y - c0.y)*dx0/dx));

                    //qDebug()<<"calc"<<x0<<xf<<x1;
                    break;
                }
                //qDebug()<<x<<col;
                //cmap.allCols.append(col);

                if(c==Col::R)
                {
                    cmap.allCols[numc] = cmap.allCols[numc] | ((col&0xFF)<<16);
                }
                else if(c==Col::G)
                {
                    cmap.allCols[numc] = cmap.allCols[numc] | ((col&0xFF)<<8);
                }
                else if(c==Col::B)
                {
                    cmap.allCols[numc] = cmap.allCols[numc] | (col&0xFF);
                }
                numc++;
                //qDebug()<<c<<numc<<col;
            }
            col_left = (col_left>cmap.num_cols)?(col_left - cmap.num_cols):0;

        }


      }

#if 0
    double ckeyx = cmap.keyCols[ckeyc].x;
    double nkeyx = cmap.keyCols[ckeyc+1].x;

    key_color keycs = cmap.keyCols[ckeyc];
    key_color  keycf = cmap.keyCols[ckeyc+1];

    int col_left = cmap.num_levels;


    int numc = 0;

    while (col_left)
    {
        for (int x=0;x<cmap.num_cols;x++)
        {

            float xf = 1.0*x/(cmap.num_cols - 1);

            //find an interval..
            int intval= -1;
            QColor col;

            for(int i=0;i<cmap.keyCols.length()-1;i++)
            {
                col=QColor(30,50,45);



                key_color c0 = cmap.keyCols[i];
                key_color cn = cmap.keyCols[i+1];
                double x0 = c0.x;
                double x1 = cmap.keyCols[i+1].x;

                if(xf<x0)
                {
                    col = c0.col;
                    //qDebug()<<"xf<x0";
                    break;
                }

                if(xf>x1)
                {
                    //qDebug()<<"xf>x0";
                    continue;
                    break;
                }



                double dx = x1 - x0;
                double dx0 = xf - x0;

                col.setRed( c0.col.red()+(cn.col.red() - c0.col.red())*dx0/dx);
                col.setGreen(c0.col.green()+(cn.col.green() - c0.col.green())*dx0/dx);
                col.setBlue(c0.col.blue()+(cn.col.blue() - c0.col.blue())*dx0/dx);

                //qDebug()<<"calc"<<x0<<xf<<x1;
                break;
            }
            //qDebug()<<x<<col;
            //cmap.allCols.append(col);

            cmap.allCols[numc++] = ((col.red()&0xFF)<<16) | ((col.green()&0xFF)<<8) | ((col.blue()&0xFF));


        }
        col_left = (col_left>cmap.num_cols)?(col_left - cmap.num_cols):0;

    }



    for(int i=0;i<cmap.keyCols.length();i++)
    {
        //qDebug()<<i<<cmap.keyCols[i].x << cmap.keyCols[i].col;

    }

    for(int i=0;i<cmap.num_levels;i++)
    {
       // qDebug()<<i << cmap.allCols[i];

    }

#endif

}

void colormap::updateHistogram(quint32*vals, quint32 num){
    qDebug()<<".."<<vals[0]<<num<<"..";
    ui->histogram->drawHistogram(vals,num);
}

void colormap::update_color_map()
{
    create_mid_colors();

    QPixmap pix;

    pix = QPixmap(ui->colmap->width(), ui->colmap->height());
    pix.fill( Qt::white );

    QPainter paint(&pix);
    qDebug()<<"cmap.num_levels"<<cmap.num_levels;
    qDebug()<<"cmap.allCols"<<(size_t)cmap.allCols;

    for(int xl=0;xl<cmap.num_levels;xl++)
    {
        int xr = pix.width()*(1.0*xl/cmap.num_levels);
        int wr = (int)pix.width()/cmap.num_levels;

        paint.setBrush(QBrush((cmap.allCols[xl])));
        paint.setPen(QPen((cmap.allCols[xl])));
        paint.drawRect(xr,0,wr,pix.height());
    }
    paint.end();
    // paint.drawRect(0,10,100,100);
    ui->colmap->setPixmap(pix);

    //pix = QPixmap(ui->rgbset->width(), ui->rgbset->height());
    //pix.fill( Qt::white );
    //paint.begin(&pix);
    ui->rgbset->resetCanvas(Qt::black);
    for(int c=0;c<3;c++)
    {
        double*yy=new double[cmap.keyCols[c].length()];
        double*xx=new double[cmap.keyCols[c].length()];
        for(int i=0;i<cmap.keyCols[c].length();i++)
        {
            xx[i] = cmap.keyCols[c][i].x;
            yy[i] = cmap.keyCols[c][i].y;
        }
        if(c==Col::R && ui->checkBox_R->isChecked())
        {
           ui->rgbset->plot(xx,yy,cmap.keyCols[c].length(),"linecolor:red;size:2;marker:true");
        }
        else if(c==Col::G && ui->checkBox_G->isChecked())
        {
           ui->rgbset->plot(xx,yy,cmap.keyCols[c].length(),"linecolor:green;size:2;marker:true");
        }
        else if(c==Col::B && ui->checkBox_B->isChecked())
        {
           ui->rgbset->plot(xx,yy,cmap.keyCols[c].length(),"linecolor:blue;size:2;marker:true");
        }
    }


    emit color_map_update_event(cmap.allCols,cmap.num_levels,ui->equlize->isChecked());
}

void colormap::on_loadPreset_clicked()
{
    QString presetName=ui->presets->currentText();

    if (colormap_presets.contains(presetName))
        cmap = colormap_presets[presetName];

    update_color_map();
}

void colormap::on_horizontalSlider_valueChanged(int value)
{

}

void colormap::on_horizontalSlider_sliderMoved(int position)
{
    if(!cmap.num_levels)
        return;

    cmap.num_cols = position;
    update_color_map();
}

void colormap::on_checkBox_R_clicked()
{
    update_color_map();
}

void colormap::on_checkBox_G_clicked()
{
    update_color_map();
}

void colormap::on_checkBox_B_clicked()
{
    update_color_map();
}

void colormap::on_num_levels_valueChanged(int value)
{
    ui->sts_num_lvl->setText(QString::number(value));

    set_num_levels(value);
}

void colormap::on_equlize_clicked()
{
    emit color_map_update_event(cmap.allCols,cmap.num_levels,ui->equlize->isChecked());

}

void colormap::on_pushButton_clicked()
{
    cmap.keyCols[Col::R].clear();
    cmap.keyCols[Col::G].clear();
    cmap.keyCols[Col::B].clear();

    for(int c=0;c<3;c++)
    {
        double x,y;
        x=0.0;
        while(x<1.0)
        {

            x=x+0.2*rand()/RAND_MAX;
            if(x>1.0)
                x=1.0;
            y=1.0*rand()/RAND_MAX;

            cmap.keyCols[c].append(key_color{x,y});
        }
    }
    update_color_map();



}
