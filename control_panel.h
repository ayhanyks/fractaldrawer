#ifndef CONTROL_PANEL_H
#define CONTROL_PANEL_H

#include <QWidget>
#include <size_level.h>
#include <QWidget>
#include <imagevis.h>

namespace Ui {
class control_panel;
}

class control_panel : public QWidget
{
    Q_OBJECT

public:
    explicit control_panel(imageVis*imgvis, QWidget *parent = 0);
    ~control_panel();

private:
    Ui::control_panel *ui;
    QList<QWidget*>panels;
    imageVis*imgvis;
};

#endif // CONTROL_PANEL_H
