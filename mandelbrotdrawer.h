#ifndef MANDELBROTDRAWER_H
#define MANDELBROTDRAWER_H
#include "QThread"

#include "QPainter"
#include "QPixmap"
#include "QLabel"


//#include "xreal.h"
#include "iostream"

//using namespace HPA;
using namespace std;


class mandelbrotDrawer : public QThread
{
    Q_OBJECT

public:
 explicit mandelbrotDrawer(QWidget *parent = 0);

 double xmin, xmax, ymin, ymax;
 int W,H;

 int busy;
 int stopcurrentThread=0;
 int currentThreadBusy;
 //int xyskip;
 int maxitr;
 int mintone;
 int maxtone;



 //QLabel*myLabel;
 int*imgMap;

void run();
void restart();
void draw();

signals:
    void updateprogress(int);

};

#endif // MANDELBROTDRAWER_H
