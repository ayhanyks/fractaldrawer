#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <imagevis.h>
#include <colormap.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    imageVis*imgvis;
    colormap*clmap;
private slots:
    void menu_select(QAction*);
};

#endif // MAINWINDOW_H
