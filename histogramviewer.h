#ifndef HISTOGRAMVIEWER_H
#define HISTOGRAMVIEWER_H

#include <QObject>
#include <imagevis.h>
#include <clicklabel.h>


class histogramViewer : public QObject
{
    Q_OBJECT
public:
    explicit histogramViewer(imageVis*imgvis, QObject *parent = nullptr);

signals:

public slots:

private slots:
    void updateHistogram(quint8 *imgbuff);
    void updateHistogram();


private:
    clickLabel*histlb;
    imageVis*imgvis;
};

#endif // HISTOGRAMVIEWER_H
