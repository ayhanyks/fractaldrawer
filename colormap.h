#ifndef COLORMAP_H
#define COLORMAP_H

#include <QWidget>
#include <QColormap>
#include <QFile>

typedef struct
{
    double x;
    double y;
}key_color;

namespace Col {
    enum{
        R,
        G,
        B
    };
}

typedef struct
{
    QList<key_color>keyCols[3];

    quint32*allCols;
    quint32 num_cols;
    quint32 num_levels;
    quint32*col_list_quint32;

}colormap_type;

namespace Ui {
class colormap;
}

class colormap : public QWidget
{
    Q_OBJECT

public:
    explicit colormap(QWidget *parent = 0);
    ~colormap();

    void set_num_levels(int numl);
    void saveFile(QFile&f);
    void loadFile(QFile&f);


private slots:
    void on_loadPreset_clicked();

    void on_horizontalSlider_valueChanged(int value);

    void on_horizontalSlider_sliderMoved(int position);
    void rgbset_touch(double x, double y);
    void rgbset_untouch(double x, double y);

    void rgbset_move(double x, double y);
    void update_color_map();
    void on_checkBox_R_clicked();

    void on_checkBox_G_clicked();

    void on_checkBox_B_clicked();

    void on_num_levels_valueChanged(int value);
    void updateHistogram(quint32 *vals, quint32 num);

    void on_equlize_clicked();

    void on_pushButton_clicked();

private:
    Ui::colormap *ui;
    QHash<QString, colormap_type>colormap_presets;
    void create_mid_colors();
    colormap_type cmap;
    int selected_keycolor_index;
    int selected_keycolor_color;

protected:
    void resizeEvent(QResizeEvent *event);

signals:
    void color_map_update_event(quint32*,quint32,bool);

};

#endif // COLORMAP_H
