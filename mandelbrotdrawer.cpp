#include "mandelbrotdrawer.h"
#include "QDebug"


mandelbrotDrawer::mandelbrotDrawer(QWidget *parent) : QThread(parent)
{
    maxitr=32;
    currentThreadBusy=0;
    maxtone=0;
    W=512;
    H=512;
    imgMap=new int[W*H];
}

void mandelbrotDrawer::run(){



    double xmint,xmaxt,ymint,ymaxt;
    xmint=xmin;
    ymint=ymin;
    xmaxt=xmax;
    ymaxt=ymax;

    cout<<"(ymax-ymin)/(xmax-xmin)"<<(ymax-ymin)/(xmax-xmin)<<endl;
    /*
    qDebug()<<"enter thread";
    if(currentThreadBusy)
    {
        while(currentThreadBusy);
        myPix->fill(Qt::black);
        cl->setPixmap(*myPix);
        stopcurrentThread=1;

    }
    */
    currentThreadBusy=1;



    double xx;
    double yy;
    double xtemp;
    double x,y;
    double steps[256];
    int px,py;
    int max_iteration=maxitr;
    int iteration;




    double rsquare, isquare,zsquare;


    cout<<xmint<<","<<xmaxt<<":"<<ymint<<","<<ymaxt<<endl;
    int maxtonex=0;
    int mintonex=256;
    int tone=0;



    double XN,YN;
    XN=(xmaxt-xmint)/W;
    YN=(ymaxt-ymint)/H;


    unsigned int index=0;

    for(px=0; px<W;px++)
    {

        emit updateprogress((px*100)/W);

        if(stopcurrentThread)
            goto endthisone;

        xx=xmin+px*XN;


        for(py=0; py<H; py++)
        {


            yy=ymin+py*YN;

            x=0;
            y=0;

            rsquare = 0;
            isquare = 0;
            zsquare = 0;
            iteration=0;

            /*
             *
             * xx=x0+p(x)
             * yy=y0+p(y)
             *
             *
             * OR
             * sum(aaa,bbb)
             *
             * sqr(xxx)
             * sqr(yyy)
             *
             * format
             * x/y:
             * (0.000~1.000)*2^(-zoom);
             *
             */

            while ((rsquare + isquare) <= 4 && iteration < (max_iteration-1)){

                x=rsquare - isquare +xx;
                //y=zsquare - rsquare - isquare + yy;     //2xy+yy
                y=zsquare+yy;

                rsquare=x*x;
                isquare=y*y;

                //zsquare=(x+y)*(x+y);//r^2 + i^2 +2xy
                zsquare=2*x*y;

                //steps[iteration]=(rsquare + isquare);
                iteration++;

             }


            //color=QColor(itr,itr,itr);
            //qDebug()<<"x:"<<px<<"y:"<<py<<"color:"<<itr;

//            QPoint p1;

#if 0
            xreal xxxx,yyyy;
            xxxx=xx*xx;
            yyyy=yy*yy;

            quint32 xd,yd,zd;

            xd=(rsquare._2double()*10);
            yd=(isquare._2double()*10);
            zd=(zsquare._2double()*10);
            if(xd>255)
                xd=255;
            if(yd>255)
                yd=255;
            if(zd>255)
                zd=255;

            paintpen.setColor(QColor(xd,yd,zd));
#else
 //           tone=iteration;
/*
            if(tone>maxtonex)
                maxtonex=tone;
            if(tone<mintonex)
                mintonex=tone;

            if(maxtone)
                tone=(255*(tone-mintone))/(maxtone-mintone);
*/
            imgMap[index++]=iteration;

           // qDebug()<<tone;
           // paintpen.setColor(QColor(tone,tone,tone));
            //qDebug()<<tone;
#endif
            //p1.setX(px);
            //p1.setY(py);

            //painter->setPen(paintpen);

            //for(int sx=0;sx<step;sx++)
            //    for(int sy=0;sy<step;sy++)
            //        painter->drawPoint(QPoint(px+sx,py+sy));



        }


         //cl->setPixmap(*myPix);
    }

    //mintone=mintonex;
    //maxtone=maxtonex;
    //qDebug()<<"mintone:"<<mintone;
    //qDebug()<<"maxtone:"<<maxtone;
#if 0
    for(px=0; px<X;px+=step)
    {
        for(py=0; py<Y; py+=step)
        {
            QPoint p1;

            tone=imgMap[Y*px+py];

            tone=(255*(tone-mintone))/(maxtone-mintone);

            paintpen.setColor(QColor(tone,tone,tone));
            painter->setPen(paintpen);

            for(int sx=0;sx<step;sx++)
                for(int sy=0;sy<step;sy++)
                    painter->drawPoint(QPoint(px+sx,py+sy));

        }


    }
#endif

    //cl->setPixmap(*myPix);
    emit updateprogress(100);

endthisone:
    qDebug()<<"exiting";


    currentThreadBusy=0;
    stopcurrentThread=0;


}



void mandelbrotDrawer::restart(){

}
