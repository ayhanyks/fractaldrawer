#include "clabel.h"
#include "qpainter.h"

clabel::clabel(QWidget* parent, Qt::WindowFlags f)
: QLabel(parent)
{

}
clabel::~clabel() {



}


void clabel::mouseMoveEvent(QMouseEvent *event){
    int x,y;
    x=event->x();
    y=event->y();


    qDebug()<<x<<y;

    double xx,yy;
    xx = _xleft + (_xright-_xleft)*(x*1.0)/this->width();
    yy = _ytop -  (_ytop-_ybtm)*(y*1.0)/this->height();

    emit moveEvent(xx,yy);
}

void clabel::mouseReleaseEvent(QMouseEvent* event) {

    int x,y;
    x=event->x();
    y=event->y();


    qDebug()<<x<<y;

    double xx,yy;
    xx = _xleft + (_xright-_xleft)*(x*1.0)/this->width();
    yy = _ytop -  (_ytop-_ybtm)*(y*1.0)/this->height();

    emit untouchEvent(xx,yy);


}

void clabel::mousePressEvent(QMouseEvent* event) {

    //emit mousedown(event->x,event->y);

   //butun buton widgetleri tek tek bak ve herhangi birine tiklanmis mi bak.
   //belki: saydam renge sahip ise tiklama kabul edilmeyebilir..

   int x,y;
   x=event->x();
   y=event->y();


   qDebug()<<x<<y;

   double xx,yy;
   xx = _xleft + (_xright-_xleft)*(x*1.0)/this->width();
   yy = _ytop -  (_ytop-_ybtm)*(y*1.0)/this->height();

   emit touchEvent(xx,yy);



}


void clabel::setcorners(double xleft, double ybtm, double xright, double ytop){
    _xleft = xleft;
    _ybtm = ybtm;
    _xright = xright;
    _ytop = ytop;
}


void clabel::drawColMap(quint32*colmap, quint32 numc)
{


    int ci;
    QPixmap pix;
    pix = QPixmap(this->width(), this->height());
    pix.fill( Qt::black );
    QPainter paint(&pix);

    int xleft = 0;
    int xright=0;
    paint.setPen(Qt::NoPen);
    for(ci = 0; ci<numc;ci++){

        xleft = xright;
        xright = (ci+1)*this->width()/numc;
        QRect r(xleft,0,xright-xleft,this->height());
        int cr = (colmap[ci]>>16) & 0xFF;
        int cg = (colmap[ci]>>8) & 0xFF;
        int cb = (colmap[ci]) & 0xFF;

        paint.setBrush(QBrush(QColor(QRgb(colmap[ci]))));

        //qDebug()<<cr<<cg<<cb;// QString::number(colmap[ci],16);
        paint.drawRect(r);

    }
     this->setPixmap(pix);
}

void clabel::drawHistogram(quint32 *hist, quint32 numlevels){
    QPixmap pix;
    //qDebug()<<"DRAWING HIST...";
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    if(!this->pixmap()==NULL /*->isNull()*/)
    {
        pix = QPixmap::fromImage(this->pixmap()->toImage());
#else
    if(!this->pixmap().isNull())
    {
        pix = QPixmap::fromImage(this->pixmap().toImage());
#endif

        //        (QPixmap*)this->pixmap();

    }
    else
    {
        pix = QPixmap(this->width(), this->height());

    }
    QPainter paint(&pix);

    pix.fill( Qt::gray );
    //int numlevels=256;
    int max_hist_level = 0;
    for(int x=0;x<numlevels;x++)
    {
        if(max_hist_level<hist[x])
            max_hist_level=hist[x];
    }
    for(int x=0;x<numlevels;x++)
    {

        int recxstart = x*this->width()/numlevels;
        int recxend = (x+1)*this->width()/numlevels;
        int recystart = this->height() - ((this->height()*hist[x]) / max_hist_level);
        int recyend = this->height() - ((this->height()*hist[x]) / max_hist_level);
        //qDebug()<<recxstart<<((this->height()*hist[x]) / max_hist_level);
        paint.drawRect(recxstart, recystart ,this->width()/numlevels,((this->height()*hist[x]) / max_hist_level));
    }
    this->setPixmap(pix);
}

void clabel::resetCanvas(QColor bgCol){
    QPixmap pix;
    pix = QPixmap(this->width(), this->height());
    pix.fill(bgCol);
    this->setPixmap(pix);
}

void clabel::plot(double *X, double *Y, quint32 numpoints, QString args){

    QPixmap pix;
    qDebug()<<this;
    qDebug()<<this->pixmap();

    pix = QPixmap::fromImage(this->pixmap()->toImage());

    //qDebug()<<this->width()<< this->height();
    QPainter paint(&pix);
    //QPainter paint()


    //pix.fill( Qt::black );

    QStringList arglist=args.split(";");

    QHash<QString,QString>options;
    options["linecolor"]="red";
    options["clean"]="false";
    options["linestyle"]="line";
    options["size"]="1";

    if (arglist.length())
    {
        foreach (QString arg, arglist) {

            if(arg.indexOf(':')>=0)
            {
                QString key = arg.split(":").at(0);
                QString val = arg.split(":").at(1);
                //qDebug()<<"options["<<key<<"]="<<val;
                options[key]=val;
            }
        }
    }

    qDebug()<<options;
    QPen pen;
    pen.setWidth(options["size"].toUInt());

    if(options["linecolor"]=="red")
    {
        pen.setColor(QColor(255, 0, 0, 255));
    }
    else if(options["linecolor"] == "green")
    {
        pen.setColor(QColor(0, 255,  0, 255));
    }
    else if(options["linecolor"] == "blue")
    {
        pen.setColor(QColor(0, 0,  255, 255));
    }


    paint.setPen(pen);
    //qDebug()<<numpoints<<"to draw";
    for(int i=0;i<numpoints;i++)
    {
        double px, py;
        quint32 xw,yw;
        quint32 xw0, yw0;

        if(X)
        {
            px = X[i];
        }
        else
        {
            px = i;
        }

        py = Y[i];

        xw = this->width()*(px - _xleft)/(_xright - _xleft);
        yw =this->height()*(1 - (py - _ybtm)/(_ytop - _ybtm));

        if(options["linestyle"]=="line")
        {
            if(i)
            {
                paint.drawLine(xw0,yw0,xw,yw);
            }
            xw0 = xw;
            yw0 = yw;
         }
        else if(options["linestyle"]=="points")
        {
            paint.drawPoint(xw,yw);

        }

        if(options["marker"]=="true")
        {
          paint.fillRect(xw-4,yw-4,8,8,QBrush(pen.color()));
        }

    }

     this->setPixmap(pix);
}
