#include "clicklabel.h"
#include "QDebug"
#include "imagevis.h"

#define max(a,b) (a>b?a:b)

clickLabel::clickLabel(QWidget* parent, Qt::WindowFlags f)
    : QLabel(parent)
{

    this->setMouseTracking(true);
}


clickLabel::~clickLabel() {


}

void clickLabel::resizeEvent(QResizeEvent *event)
{
    emit resized();
}

void clickLabel::mouseDoubleClickEvent(QMouseEvent *ev){
    if(ev->button()==1)
        emit zoomIn(ev->x(),ev->y());
    qDebug()<<"mouseDoubleClickEvent"<<ev->x()<<ev->y()<<ev->button();
}

//void clickLabel::ho

void clickLabel::updateImg(quint8 *imgbuff){

    int w = this->width();
    int h = this->height();

    img = QImage(imgbuff,w,h,w*3,QImage::Format_RGB888);
    this->setPixmap(QPixmap::fromImage(img));

    //also update histogram..
}

void clickLabel::updateHistogram(quint8 *imgbuf){
    static int q=0;
    qDebug()<<"updateHistogram call"<<q++;

    //draw histogram/
    imageVis*imgvis=(imageVis*)sender();

    QPixmap pix;
    pix = QPixmap(this->width(), this->height());
    pix.fill( Qt::white );
    QPainter paint(&pix);

    quint32*hist=imgvis->getHistogram();
    int max_hist_level = 0;
    for(int x=0;x<imgvis->histogramGetLevels();x++)
    {
        if(max_hist_level<hist[x])
            max_hist_level=hist[x];
    }
    for(int x=0;x<imgvis->histogramGetLevels();x++)
    {

        int recxstart = x*this->width()/imgvis->histogramGetLevels();
        int recxend = (x+1)*this->width()/imgvis->histogramGetLevels();
        int recystart = this->height() - ((this->height()*hist[x]) / max_hist_level);
        int recyend = this->height() - ((this->height()*hist[x]) / max_hist_level);
        qDebug()<<recxstart<<((this->height()*hist[x]) / max_hist_level);
        paint.drawRect(recxstart, recystart ,this->width()/imgvis->histogramGetLevels(),((this->height()*hist[x]) / max_hist_level));
    }

    this->setPixmap(pix);
}



void clickLabel::mouseMoveEvent(QMouseEvent *ev){



    int dx;
    int dy;

    //qDebug()<<"mouse Move"<<ev->button();

    if(gestureMode ==1)
    {
        if(oldxyValid)
        {
            dx = ev->x() - oldx;
            dy = ev->y() - oldy;
            //dx = 0;

            total_panx +=dx;
            total_pany +=dy;

           // emit mouseMove(dx,dy);

        }
        oldx = ev->x();
        oldy = ev->y();
        oldxyValid = true;


        int H=this->height();
        int W=this->width();

        int dstx,dsty,srcx,srcy;
        int w,h;
        int drawy0;
        int drawyh;

        QPixmap*pmap = new QPixmap(W,H);
        QPainter *painter = new QPainter(pmap);
        dstx = total_panx>0?total_panx:0;
        srcx = total_panx>0?0:(-total_panx);
        dsty = total_pany>0?total_pany:0;
        srcy = total_pany>0?0:(-total_pany);
        w  = total_panx>0?W-total_panx:W+total_panx;
        h = total_pany>0?H-total_pany:H+total_pany;;
        pmap->fill(Qt::black);
        painter->drawPixmap(dstx,dsty,w,h,refpixmap.copy(srcx,srcy,w,h));
        this->setPixmap(pmap->scaled(W,H));
    }

    emit(mousehover(ev->x(),ev->y()));
#if 0
    if(gestureMode==1)
    {
         zoomrecx1=ev->x();
         zoomrecy1=ev->y();

          QLabel*zoomlabel=this->findChild<QLabel*>("zoomlabel");


          //keep aspect ratio
          int wd=this->width();
          int hg=this->height();

          int zw=abs(zoomrecx1-zoomrecx0);
          int zh=(zw*hg)/wd;


          zoomlabel->setGeometry((zoomrecx0>zoomrecx1)?zoomrecx1:zoomrecx0,
                                 (zoomrecy0>zoomrecy1)?zoomrecy1:zoomrecy0,
                                 zw,
                                 zh
                                 );
          zoomlabel->show();

    }
#endif



}

#if 1
void clickLabel::mouseReleaseEvent(QMouseEvent *e){

    qDebug()<<"mouse released"<<e->button()<<total_panx<<total_pany;

    if(total_panx || total_pany)
        emit pan(total_panx,total_pany);

    total_panx = 0;
    total_pany = 0;
    oldxyValid =false;
    gestureMode = 0;

}
#endif


void clickLabel::mousePressEvent(QMouseEvent *ev){


    qDebug()<<"mouse press"<<ev->button();
    if(ev->button()==1)
    {
        total_panx = 0;
        total_pany = 0;
        oldxyValid =false;
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        refpixmap = this->pixmap()->copy(0,0,this->width(),this->height());
#else
        refpixmap = this->pixmap().copy(0,0,this->width(),this->height());
#endif
        gestureMode = 1;
    }
    else if(ev->button()==2)
    {
       emit zoomOut(ev->x(),ev->y());
       gestureMode = 2;
    }


    //QImage img(refpixmap.toImage());


#if 0
    if(ev->button()==1)
        emit leftclick(ev->x(),ev->y());
    else if(ev->button()==2)
        emit rightclick(ev->x(),ev->y());
#endif
    /*
    gestureMode=1;
    zoomrecx0=ev->x();
    zoomrecx1=ev->x();
    zoomrecy0=ev->y();
    zoomrecy1=ev->y();

    QLabel*zoomlabel=new QLabel(this);
    zoomlabel->setObjectName("zoomlabel");
    zoomlabel->setStyleSheet("background-color: rgba(222, 32, 44,128);");

    zoomlabel->hide();
    */
}
