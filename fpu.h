#ifndef FPU_H
#define FPU_H
#include "stdint.h"

typedef struct WORKLOAD{

    uint32_t W;
    uint32_t H;

    uint32_t xstart;
    uint32_t xend;
    uint32_t ystart;
    uint32_t yend;

    double xc;
    double yc;
    int zoom;

    uint32_t*outputBuf;
    uint32_t*outputBuf2;
    uint32_t*colMap;
    uint32_t*histogram;
    uint32_t numlevels;
    bool hist_eq;
    void*rgbBuf;
    void (*callback)(WORKLOAD*);

    bool done;

}WORKLOAD;

void fpu_startWorkload(WORKLOAD*W);

class fpu
{
public:
    fpu();
};

#endif // FPU_H
