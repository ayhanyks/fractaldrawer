#include "control_panel.h"
#include "ui_control_panel.h"



control_panel::control_panel(imageVis*imgvis, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::control_panel)
{

    this->imgvis = imgvis;
    ui->setupUi(this);

    panels.append(new size_level(imgvis, this));

    ui->toolBox->removeItem(0);

    foreach (QWidget*cpi, panels) {
        ui->toolBox->addItem(cpi,cpi->objectName());


    }


}

control_panel::~control_panel()
{
    delete ui;
}
