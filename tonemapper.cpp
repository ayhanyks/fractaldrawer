#include "tonemapper.h"
#include "ui_tonemapper.h"
#include "clabel.h"
#include "qpainter.h"

tonemapper::tonemapper(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::tonemapper)
{
    ui->setupUi(this);

    cl = new clabel(ui->frame_2);
    cl->setGeometry(20,20,ui->frame_2->width()-40, ui->frame_2->height()-40);
    cl->setStyleSheet("background-color: rgb(0, 0, 0);");
    connect(cl,SIGNAL(touchEvent(double,double)), this,SLOT(selectColor(double,double)));
    connect(cl,SIGNAL(moveEvent(double,double)), this, SLOT(moveColor(double, double)));
    connect(cl,SIGNAL(untouchEvent(double,double)), this, SLOT(updateColor(double, double)));
    cl->show();


    ctm =  new clabel(ui->frame_3);
    ctm->setGeometry(20,20,ui->frame_3->width()-40, ui->frame_3->height()-40);
    ctm->setStyleSheet("background-color: rgb(0, 0, 0);");
    ctm->show();




    keycolors[COLRED].append({0,0});
    keycolors[COLRED].append({32,0x30});
    keycolors[COLRED].append({88,0x40});
    keycolors[COLRED].append({255,0xFF});

    keycolors[COLGREEN].append({0,0});
    keycolors[COLGREEN].append({64,0x30});
    keycolors[COLGREEN].append({100,0x40});
    keycolors[COLGREEN].append({255,0xFF});

    keycolors[COLBLUE].append({0,0});
    keycolors[COLBLUE].append({32,0xFF});
    keycolors[COLBLUE].append({196,0x40});
    keycolors[COLBLUE].append({255,0xFF});



    colmap_r = new quint8[256];
    colmap_g = new quint8[256];
    colmap_b = new quint8[256];

    selectedColor = 0;
    selColor = COLNONE;
    selKeyPoint=-1;
    histogram = 0;

}


tonemapper::~tonemapper()
{
    delete ui;
}


void tonemapper::updateColor(double x, double y)
{
     memcpy(colmap_r, selectedColor,256);
     delete selectedColor;
     selectedColor = 0;
     selectedX = -1;
     drawTonemap(tm);
}

void tonemapper::selectColor(double x, double y){

   //qDebug()<<"select color at"<<x<<y;

   //for example, select blue..
   selectedColor= new quint8[256];
   memcpy(selectedColor,colmap_r,256);


   selectedX=(quint32)x;
   selectedY=(quint8)selectedColor[selectedX];

   selColor = COLNONE;
   selKeyPoint=-1;

   for(int ci=COLRED;ci<COLNONE;ci++)
   {
        for(int kpi=0;kpi<keycolors[ci].length();kpi++)
        {
            int kpx=keycolors[ci].at(kpi).x;
            int kpc=keycolors[ci].at(kpi).c;

            if(abs(kpx-(int)x)<5 && (abs(kpc-(int)y)<5))
            {
                selColor=ci;
                selKeyPoint=kpi;
                break;
            }

        }
        if(selColor!=COLNONE)
        {
            break;
        }

        //maybe a key point need to be added?
        int cpoint = getcolor((int)x, ci);
        if(abs(cpoint-(int)y)<5)
        {

            //add a key point..

            selColor=ci;
            selKeyPoint=addKeyPoint(x,cpoint,(COLOR)ci);
            break;
        }


   }
   drawTonemap(tm);



}


void tonemapper::moveColor(double x, double y)
{
    //qDebug()<<"moving"<<x<<y;

#if 0
    //is there a key color around?

    //move other colors around too
    int dy = (int)y - selectedY ;

    int span = 50;


    for(int s=0;s<=256;s++)
    {
        double dx =abs((int)selectedX - s);
        int newcol = (int)colmap_r[s]+ (int)(dy/(1.0+(dx*dx/10)));

        newcol=newcol>255?255:(newcol<0?0:newcol);
        selectedColor[s]=newcol;

    }

#endif




    if(selColor>=0)
    {
        if(selKeyPoint>=0)
        {


            int yint = (int)y;
            int xint = (int)x;
            int x0 = (int)keycolors[selColor][selKeyPoint].x;
            int dx = xint -  (int)keycolors[selColor][selKeyPoint].x;

            qDebug()<<"yint: "<<yint;
            if(yint<-50 || yint>300)
            {
                keycolors[selColor].removeAt(selKeyPoint);
                selColor = COLNONE;
                selKeyPoint = -1;
                drawTonemap(tm);
                return;
            }

            yint=yint>255?255:(yint<0?0:yint);
            keycolors[selColor][selKeyPoint].c=(quint8)yint;


            xint=xint>255?255:(xint<0?0:xint);
            keycolors[selColor][selKeyPoint].x =(quint32)xint;

            if(dx>0)
            {
                for(int si=selKeyPoint+1;si<keycolors[selColor].length();si++)
                {
                    if(keycolors[selColor][si].x <= keycolors[selColor][selKeyPoint].x )
                    {

                        keycolors[selColor][selKeyPoint].x = keycolors[selColor][si].x-1;
                        break;

                        /*
                        if((keycolors[selColor][selKeyPoint].x - keycolors[selColor][si].x)>10)
                        {
                            keycolors[selColor].removeAt(si);
                            break;
                        }
                        else
                        {
                            keycolors[selColor][selKeyPoint].x = keycolors[selColor][si].x-1;
                            break;
                        }
                        */

                    }
                }
            }
            else if(dx<0)
            {
                for(int si=0;si<selKeyPoint;si++)
                {
                    if(keycolors[selColor][si].x >= keycolors[selColor][selKeyPoint].x )
                    {

                        keycolors[selColor][selKeyPoint].x = keycolors[selColor][si].x+1;
                        break;
                        /*
                        if((keycolors[selColor][si].x - keycolors[selColor][selKeyPoint].x)>10)
                        {
                            keycolors[selColor].removeAt(si);
                            selKeyPoint--;
                            break;
                        }
                        else
                        {
                            keycolors[selColor][selKeyPoint].x = keycolors[selColor][si].x+1;
                            break;
                        }
                        */


                    }
                }
            }
        }

        //
    }

    drawTonemap(tm);
}

int tonemapper::addKeyPoint(quint32 x, quint8 c, COLOR COL){

    for(int kpi=0;kpi<keycolors[COL].length();kpi++)
    {
        if(keycolors[COL].at(kpi).x>x)
        {
            keycolors[COL].insert(kpi,{x,c});
            return kpi;
        }
    }
    //add to end
    keycolors[COL].append({x,c});
    return keycolors[COL].length()-1;
}

quint32 tonemapper::getcolor(quint32 x, int COL)
{
    //find two key colors which x is in between
    //calculate by linear interpolation
    //TODO: use other onterpolation methods..

    if(keycolors[COL].length()==0)
        return 0;

    if(x<=keycolors[COL].first().x)
        return keycolors[COL].first().c;

    if(x>=keycolors[COL].last().x)
        return keycolors[COL].last().c;


    for(int i=0;i<keycolors[COL].length();i++)
    {
        keycolor keycol = keycolors[COL].at(i);
        if(x==keycol.x)
           return keycol.c;


        if(i>0 && keycolors[COL].at(i-1).x < x && keycol.x>x)
        {
            //linear interpolation
            quint8 c0 =  keycolors[COL].at(i-1).c;
            quint32 x0 = keycolors[COL].at(i-1).x;

            quint8 c1 =  keycolors[COL].at(i).c;
            quint32 x1 = keycolors[COL].at(i).x;

            return (c0 +  ((c1-c0)*(float)(x-x0))/((float)(x1-x0)));

        }
    }
    //qDebug()<<"no value returned";
    return 0;

}

void tonemapper::drawTonemap(tonemap*tm)
{

    //generate based on key colors

    for(quint32 i=0; i<256;i++)
    {
        colmap_r[i] = getcolor(i,COLRED);
        colmap_g[i] = getcolor(i,COLGREEN);
        colmap_b[i] = getcolor(i,COLBLUE);

        tonemapping[i] = (colmap_r[i]<<16) | (colmap_g[i]<<8) | (colmap_b[i] );
    }


    cl->setcorners(-10,-10,266,266);

    if(this->histogram)
    {
        cl->drawHistogram(this->histogram,256);
    }

    double*x=new double[256];
    double*c=new double[256];

    for(int i=0;i<256;i++)
    {
        x[i] = (double)i;
    }


    for(int i=0;i<256;i++){
           c[i]=(double)colmap_r[i];
    }
    cl->plot(x,c,256,"linecolor:red");




    for(int i=0;i<256;i++){
           c[i]=(double)colmap_g[i];
    }
    cl->plot(x,c,256,"linecolor:green");




    for(int i=0;i<256;i++){
           c[i]=(double)colmap_b[i];
    }
    cl->plot(x,c,256,"linecolor:blue");





    for(int i=0;i<keycolors[COLRED].length();i++)
    {
        x[i] = (double)keycolors[COLRED].at(i).x;
        c[i] = (double)keycolors[COLRED].at(i).c;
    }
    cl->plot(x,c,keycolors[COLRED].length(),"linecolor:red;linestyle:points;size:8");

    for(int i=0;i<keycolors[COLBLUE].length();i++)
    {
        x[i] = (double)keycolors[COLBLUE].at(i).x;
        c[i] = (double)keycolors[COLBLUE].at(i).c;
    }
    cl->plot(x,c,keycolors[COLBLUE].length(),"linecolor:blue;linestyle:points;size:8");

    for(int i=0;i<keycolors[COLGREEN].length();i++)
    {
        x[i] = (double)keycolors[COLGREEN].at(i).x;
        c[i] = (double)keycolors[COLGREEN].at(i).c;
    }
    cl->plot(x,c,keycolors[COLGREEN].length(),"linecolor:green;linestyle:points;size:8");


    ctm->drawColMap(tonemapping,256);

    emit updateMap(tonemapping,256);
}

void tonemapper::on_pushButton_3_clicked()
{
    drawTonemap(tm);
}
