#include "fpu.h"
#include "stdio.h"
#include <stdlib.h>

#include <thread>         // std::thread
#include "math.h"

#include <gmp.h>
#include <mpfr.h>

#define NUMTHREADS  16
#define MANDELBROT_MAXITR_DEFAULT 256

std::thread threads[NUMTHREADS];


fpu::fpu()
{

}
static uint32_t mandelbrot_mpfr(double x, double y,int max_itr=MANDELBROT_MAXITR_DEFAULT)
{
    mpfr_t xn,yn;
    mpfr_t rsq;
    mpfr_t isq;
    mpfr_t zsq;

    mpfr_init2(xn,128);
    mpfr_init2(yn,128);
    mpfr_init2(rsq,128);
    mpfr_init2(isq,128);
    mpfr_init2(zsq,128);


    uint32_t itr=0;
    /*
    while (itr < (max_itr-1)) {
        mpfr_add(msq, rsq,isq);
        mpfr_
    }
    */
    /*
    while (  (rsq + isq) <= 4 && itr < (max_itr-1)){

        xn=rsq - isq +x;
        //y=zsquare - rsquare - isquare + yy;     //2xy+yy
        yn=zsq+y;

        rsq=xn*xn;
        isq=yn*yn;

        //zsquare=(x+y)*(x+y);//r^2 + i^2 +2xy
        zsq=2*xn*yn;

        //steps[iteration]=(rsquare + isquare);
        itr++;

    }
    */

    return itr;
}


static uint32_t mandelbrot(long double x, long double y,int max_itr=MANDELBROT_MAXITR_DEFAULT)
{

    long double xn, yn;
    long double rsq=0.0;
    long double isq=0.0;
    long double zsq=0.0;

    uint32_t itr=0;

    while ((rsq + isq) <= 4 && itr < (max_itr-1)){

        xn=rsq - isq +x;
        //y=zsquare - rsquare - isquare + yy;     //2xy+yy
        yn=zsq+y;

        rsq=xn*xn;
        isq=yn*yn;

        //zsquare=(x+y)*(x+y);//r^2 + i^2 +2xy
        zsq=2*xn*yn;

        //steps[iteration]=(rsquare + isquare);
        itr++;

    }

    if(itr==(max_itr-1))
    {
        return itr;
    }
    else
    {
        uint8_t ratio = int(255.0*4.0/(rsq+isq));
        return  (ratio<<16) | itr;
    }
    //return itr;

}


void wloadThread(WORKLOAD*W){

   // printf("worker thread Y = %d -- %d X=%d -- %d\n",W->ystart, W->yend, W->xstart, W->xend);
   // printf("W=%d H=%d\n", W->W, W->H);
   // printf("XC:%0.3f YC=%0.3f\n",W->xc, W->yc );
   // printf("ZOOM=%d\n", W->zoom);
   // printf("obuf=0x%08x\n",W->outputBuf);

    long double x,y;

    uint32_t xi,yi;



    long double ar=1.0*W->W/W->H;

    long double height=pow(2,W->zoom);
    long double width=pow(2,W->zoom)*ar;

    long double ybtm = W->yc  - (height/2.0);
    long double xleft = W->xc - (width/2.0);

    long double ystep = height/(long double)W->H;
    long double xstep = width /(long double)(W->W);
    uint32_t outOffset;

    for(yi=W->ystart; yi<W->yend;yi++)
    {
        y = ybtm + ystep*(long double)yi;
        outOffset = W->W * yi+W->xstart;

        for(xi=W->xstart;xi<W->xend;xi++)
        {
            x = xleft + xstep*(double)xi;
            //if(W->xstart)
             //   W->outputBuf[outOffset]=128;
            //else
            W->outputBuf[outOffset] = mandelbrot(x,y,W->numlevels);
            outOffset++;
        }
    }

   // W->callback(W);


}



void fpu_startWorkload(WORKLOAD*W){
    printf("start workload Z=%d\n",W->zoom);



    int i;
    WORKLOAD*w;
    uint32_t dx,dy;

    int winh=W->yend - W->ystart;
    int winw=W->xend - W->xstart;

    if(winh>=winw)
    {
        dy = winw/NUMTHREADS;
        for(i=0;i<NUMTHREADS;i++)
        {
            w=(WORKLOAD*)malloc(sizeof(WORKLOAD));
            *w=*W;

            w->ystart = W->ystart + (i*winh/NUMTHREADS);
            w->yend = w->ystart + (winh/NUMTHREADS);

            if(i==0)
                w->ystart = W->ystart;
            else
                w->ystart = W->ystart + i*dy;

            if(i==(NUMTHREADS-1))
                w->yend = W->yend;
            else
                w->yend = w->ystart + dy;

            threads[i]=std::thread(wloadThread,w);

        }
    }
    else
    {
        dx = winw/NUMTHREADS;

        for(i=0;i<NUMTHREADS;i++)
        {
            w=(WORKLOAD*)malloc(sizeof(WORKLOAD));
            *w=*W;

             //(i*winw/NUMTHREADS);

            if(i==0)
                w->xstart = W->xstart;
            else
                w->xstart = W->xstart + i*dx;

            if(i==(NUMTHREADS-1))
                w->xend = W->xend;
            else
                w->xend = w->xstart + dx;

            threads[i]=std::thread(wloadThread,w);

        }
    }
    for(i=0;i<NUMTHREADS;i++)
    {
        threads[i].join();
    }



}
