#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <clicklabel.h>
#include <qscrollarea.h>
#include <QDebug>

#define XRES    (1024)
#define YRES    (512)
#define DEFAULT_NUM_LVL (256)
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    clickLabel*cl=new clickLabel(ui->frame);
    QScrollArea*qsa=new QScrollArea(ui->frame);
    qsa->setBackgroundRole(QPalette::Dark);
    QHBoxLayout*qhbl=new QHBoxLayout();
    ui->frame->setLayout(qhbl);
    imgvis=new imageVis(qsa);
    imgvis->setup(XRES,YRES,DEFAULT_NUM_LVL);
    qsa->setWidget(imgvis->getScren());
    qhbl->addWidget(qsa);


    clmap=new colormap(ui->page);
    clmap->set_num_levels(DEFAULT_NUM_LVL);
    connect(clmap,SIGNAL(color_map_update_event(quint32*,quint32,bool)), imgvis,SLOT(updateColorMap(quint32*,quint32,bool)));
    clmap->show();

    connect(ui->menubar,SIGNAL(triggered(QAction*)),this,SLOT(menu_select(QAction*)));


    QLabel*sts_lbl=new QLabel();
    sts_lbl->setVisible(true);
    //sts_lbl->setGeometry(0,0,ui->statusbar->width(),ui->statusbar->height());
    connect(imgvis,SIGNAL(statusMsg(QString)),sts_lbl,SLOT(setText(QString)));
    connect(imgvis,SIGNAL(updateHistogramSignal(quint32*,quint32)),clmap,SLOT(updateHistogram(quint32*,quint32)));

    //sts_lbl->setStyleSheet("background-color: rgb(233, 185, 110);");
    //QHBoxLayout*hl=new QHBoxLayout(ui->statusbar);
    ui->statusbar->addWidget(sts_lbl,0);



   // hl->addStretch(0);

}

MainWindow::~MainWindow()
{
    delete ui;
}




void MainWindow::menu_select(QAction *a){

    if(a->objectName()=="actionsave")
    {
        QFile file("test.test");
        file.open(QIODevice::WriteOnly);
        imgvis->saveFile(file);
        clmap->saveFile(file);
        file.close();
    }
    else if(a->objectName()=="actionopen")
    {
        QFile file("test.test");
        file.open(QIODevice::ReadOnly);
        imgvis->openFile(file);
        clmap->loadFile(file);
        file.close();
    }

}
