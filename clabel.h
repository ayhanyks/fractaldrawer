#ifndef CLABEL_H
#define CLABEL_H
#include "QMouseEvent"
#include "QDebug"
#include "QLabel"

class clabel : public QLabel
{
    Q_OBJECT
public:
    explicit  clabel(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~clabel();

    void setcorners(double xleft, double ybtm, double xright, double ytop);
    void plot(double*X, double*Y, quint32 numpoints, QString args="");
    void drawColMap(quint32 *colmap, quint32 numc);
    void drawHistogram(quint32*hist, quint32 numlevels);
    void resetCanvas(QColor bgCol);
    //QPoint getXY(double x, double y);
signals:
    void touchEvent(double x, double y);
    void untouchEvent(double x, double y);
    void moveEvent(double x, double y);

protected:
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent*event);
    void mouseMoveEvent(QMouseEvent *event);

private:
    double _xleft;
    double _ybtm;
    double _xright;
    double _ytop;


};


#endif // CLABEL_H
