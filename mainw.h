#ifndef MAINW_H
#define MAINW_H

#include <QMainWindow>
#include <clicklabel.h>
#include <QScrollArea>
#include <QHBoxLayout>
#include <imagevis.h>
#include <control_panel.h>
#include <QStringList>

class mainW : public QMainWindow
{
    Q_OBJECT
public:
    explicit mainW(QWidget *parent = nullptr);

signals:

public slots:
    void menu_resize(QAction*action);
    void menu_set_levels(QAction*action);
private:
    imageVis*imgvis;
    void createMenus();

    const QStringList menu_image{"256x256","512x256","512x512","1024x512","1024x1024","custom"};
    const QStringList menu_levels{"16","32","64","128","256","512","1024","custom"};

    clickLabel*cl;

};

#endif // MAINW_H
