#ifndef TONEMAPPER_H
#define TONEMAPPER_H

#include <QDialog>
#include "clabel.h"
//#include "QLinkedList"

typedef struct {
    quint32 numpoints;
    quint32 numkeypoints;
    quint32*keypoints;
    quint32*allpoints;
}tonemap;

typedef struct {
    quint32 x;
    quint32 c;
}keycolor;

namespace Ui {
class tonemapper;
}
enum COLOR{
    COLRED      =   0,
    COLGREEN    =   1,
    COLBLUE     =   2,
    COLNONE     =   3,


};

class tonemapper : public QDialog
{
    Q_OBJECT

public:
    explicit tonemapper(QWidget *parent = 0);
    ~tonemapper();
    void drawTonemap(tonemap *tm);
    void getColorMap(quint32 colormap);
    void setHistogram(quint32*hist){
        qDebug()<<"histogram:"<<hist;
        histogram=hist;
    }
signals:
    void updateMap(quint32*colmap, quint32 numc);

private slots:
    void on_pushButton_3_clicked();
    void selectColor(double x, double y);
    void moveColor(double x, double y);
    void updateColor(double x, double y);


private:
    Ui::tonemapper *ui;
    tonemap*tm;
    clabel*cl;
    clabel*ctm;

    quint8*colmap_r;
    quint8*colmap_g;
    quint8*colmap_b;

    QList<keycolor> keycolors[3];
    quint32 getcolor(quint32 x, int color);
    int addKeyPoint(quint32 x, quint8 c, COLOR COL);
    quint8*selectedColor;
    quint32 selectedX;
    quint8 selectedY;


    int selColor;
    int selKeyPoint;
    quint32 tonemapping[256];
    quint32*histogram;
};

#endif // TONEMAPPER_H
